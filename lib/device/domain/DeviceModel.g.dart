// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'DeviceModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$DeviceModelImpl _$$DeviceModelImplFromJson(Map<String, dynamic> json) =>
    _$DeviceModelImpl(
      licence: json['licence'] as String?,
      airQuality: json['airQuality'] as String?,
      temperature: (json['temperature'] as num?)?.toDouble(),
      humidity: (json['humidity'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$$DeviceModelImplToJson(_$DeviceModelImpl instance) =>
    <String, dynamic>{
      'licence': instance.licence,
      'airQuality': instance.airQuality,
      'temperature': instance.temperature,
      'humidity': instance.humidity,
    };
