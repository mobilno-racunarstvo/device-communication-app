import 'package:freezed_annotation/freezed_annotation.dart';

part 'DeviceModel.freezed.dart';

part 'DeviceModel.g.dart';

@freezed
class DeviceModel with _$DeviceModel {
  factory DeviceModel(
      {String? licence,
      String? airQuality,
      double? temperature,
      double? humidity}) = _DeviceModel;

  factory DeviceModel.fromJson(Map<String, dynamic> json) => _$DeviceModelFromJson(json);
}
