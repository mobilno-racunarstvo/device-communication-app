abstract class DeviceRepository {
  Future<dynamic> getData();
}