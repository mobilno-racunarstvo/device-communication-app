import 'dart:convert';

import 'package:device_connection_app/device/data/DeviceRepository.dart';
import 'package:device_connection_app/device/domain/DeviceModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class HttpDeviceRepository implements DeviceRepository {
  const HttpDeviceRepository({required this.client, required this.domain});

  final String domain;
  final http.Client client;

  @override
  Future<dynamic> getData() async {
    try {
      var response =
          await http.get(Uri.http(domain, "api/data/AQ-222222"));
      if (response.statusCode == 200) {
        return DeviceModel.fromJson(jsonDecode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      debugPrint('Error: $e');
    }
  }
}
