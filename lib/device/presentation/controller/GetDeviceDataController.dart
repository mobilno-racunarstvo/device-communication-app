import 'package:device_connection_app/device/data/DeviceRepository.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class GetDeviceDataController extends StateNotifier<AsyncValue<dynamic>> {
  GetDeviceDataController({required this.deviceRepository})
      : super(const AsyncData<void>(null)){
    getData();
  }
  final DeviceRepository deviceRepository;

  Future<dynamic> getData() async {
    state = const AsyncLoading<dynamic>();
    state = await AsyncValue.guard<dynamic>(
            () => deviceRepository.getData());
  }
}