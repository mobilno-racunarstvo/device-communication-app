import 'dart:async';

import 'package:device_connection_app/device/domain/DeviceModel.dart';
import 'package:device_connection_app/device/presentation/widget/Dashboard.dart';
import 'package:device_connection_app/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ApiExample extends ConsumerStatefulWidget {
  const ApiExample({super.key});

  @override
  ConsumerState<ApiExample> createState() => _ApiExampleState();
}

class _ApiExampleState extends ConsumerState<ApiExample> {
  late DeviceModel? _device;
  late String _errorMessage = "";
  Timer? _timer;

  @override
  void initState() {
    super.initState();
    _fetchDataPeriodically();
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  void _fetchDataPeriodically() {
    _timer = Timer.periodic(const Duration(seconds: 30), (timer) {
      ref.read(getDeviceDataControllerProvider.notifier).getData();
    });
  }

  @override
  Widget build(BuildContext context) {
    final deviceState = ref.watch(getDeviceDataControllerProvider);

    ref.listen(getDeviceDataControllerProvider, (_, state) {
      if (state.hasValue && state.value is DeviceModel) {
        setState(() {
          _device = state.value as DeviceModel;
        });
      } else {
        setState(() {
          _errorMessage = "Error while fetching data";
        });
      }
    });

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).canvasColor,
          title: Align(
            alignment: Alignment.center,
            child: Text(
              "API data",
              style: Theme.of(context).textTheme.bodyLarge,
            ),
          ),
          actions: const [
            SizedBox(
              width: 45,
            )
          ],
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => const Dashboard()));
            },
          ),
        ),
        body: Align(
          alignment: Alignment.center,
          child: deviceState.isLoading
              ? const CircularProgressIndicator(
                  color: Colors.white,
                )
              : deviceState.hasValue && deviceState.value is DeviceModel
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 10, bottom: 10),
                          child: Text("Licence: ${_device?.licence}"),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10, bottom: 10),
                          child: Text("Air quality: ${_device?.airQuality}"),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10, bottom: 10),
                          child: Text("Temperature: ${_device?.temperature}"),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10, bottom: 10),
                          child: Text("Humidity: ${_device?.humidity}"),
                        ),
                      ],
                    )
                  : Text(
                      _errorMessage,
                      style: Theme.of(context)
                          .textTheme
                          .bodyMedium
                          ?.copyWith(color: Colors.red),
                    ),
        ));
  }
}
