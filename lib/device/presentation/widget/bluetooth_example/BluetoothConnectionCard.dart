import 'package:device_connection_app/device/presentation/widget/bluetooth_example/BluetoothChat.dart';
import 'package:device_connection_app/device/service/BluetoothService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

class BluetoothConnectionCard extends StatelessWidget {
  const BluetoothConnectionCard(
      {super.key, required this.blDevice, required this.bluetoothService});

  final BluetoothDevice blDevice;
  final BluetoothService bluetoothService;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        await bluetoothService.connectToDevice(blDevice);
        if (bluetoothService.connected) {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => BluetoothChat(
                    bluetoothService: bluetoothService,
                  )));
        }
      },
      child: Card(
        margin: const EdgeInsets.all(10),
        shape: const StadiumBorder(
          side: BorderSide(
            color: Colors.black,
            width: 2.0,
          ),
        ),
        color: Theme.of(context).primaryColor,
        child: Padding(
            padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
            child: SizedBox(
              width: double.infinity,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: 280,
                    child: Text(
                      blDevice.name ?? "",
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context)
                          .textTheme
                          .bodyMedium
                          ?.copyWith(color: Theme.of(context).canvasColor),
                    ),
                  ),
                  const Icon(
                    Icons.bluetooth_connected,
                    color: Colors.white,
                    size: 30,
                  )
                ],
              ),
            )),
      ),
    );
  }
}
