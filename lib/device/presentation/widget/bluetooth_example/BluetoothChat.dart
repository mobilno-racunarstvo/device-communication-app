import 'dart:async';
import 'dart:convert';

import 'package:device_connection_app/device/presentation/widget/bluetooth_example/BluetoothMain.dart';
import 'package:device_connection_app/device/service/BluetoothService.dart';
import 'package:device_connection_app/shared/presentation/widget/TextInputField.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

class BluetoothMessage {
  const BluetoothMessage({required this.message, required this.isSent});

  final String message;
  final bool isSent;
}

class BluetoothChat extends StatefulWidget {
  const BluetoothChat({super.key, required this.bluetoothService});

  final BluetoothService? bluetoothService;

  @override
  State<BluetoothChat> createState() => _BluetoothChatState();
}

class _BluetoothChatState extends State<BluetoothChat> {
  final ScrollController _scrollController = ScrollController();
  final List<BluetoothMessage> _messages = [];
  final TextEditingController _textEditingController = TextEditingController();
  final List<String> _displayTexts = List.generate(50, (index) => "");
  final List<int> _currentIndexes = List.generate(50, (index) => 0);

  @override
  void initState() {
    super.initState();
    widget.bluetoothService?.listenForMessages((message, isSent) {
      _addMessage(message, isSent);
    });
  }

  @override
  void dispose() {
    super.dispose();
    widget.bluetoothService?.disposeConnection();
  }

  void _sendMessageToBluetooth(String message) {
    widget.bluetoothService?.sendMessage((m, isSent) {
      _addMessage(m, isSent);
    }, message);
  }

  void _addMessage(String message, bool isSent) {
    setState(() {
      _messages.add(BluetoothMessage(message: message, isSent: isSent));
    });
    _animate(_messages.length - 1);
  }

  void _animate(int i) {
    Timer.periodic(const Duration(milliseconds: 7), (timer) {
      if (_currentIndexes[i] < _messages[i].message.length) {
        setState(() {
          _displayTexts[i] =
              _messages[i].message.substring(0, _currentIndexes[i] + 1);
          _currentIndexes[i]++;
          _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
        });
      } else {
        timer.cancel();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Align(
          alignment: Alignment.center,
          child: Text("Chat"),
        ),
        backgroundColor: Theme.of(context).canvasColor,
        actions: const [
          SizedBox(
            width: 40,
          )
        ],
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => const BluetoothMain()));
          },
        ),
      ),
      body: Column(
        children: [
          Flexible(
              child: ListView.builder(
                  controller: _scrollController,
                  itemCount: _messages.length,
                  itemBuilder: (context, index) {
                    return _messages[index].isSent
                        ? Stack(
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(50, 5, 10, 15),
                                child: Container(
                                  alignment: Alignment.centerRight,
                                  padding: const EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(10)),
                                      color: Theme.of(context)
                                          .secondaryHeaderColor),
                                  child: Text(_displayTexts[index]),
                                ),
                              ),
                              Positioned(
                                  right: -15,
                                  child: Icon(
                                    Icons.arrow_right,
                                    color:
                                        Theme.of(context).secondaryHeaderColor,
                                    size: 50,
                                  ))
                            ],
                          )
                        : Stack(
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(10, 5, 50, 15),
                                child: Container(
                                  alignment: Alignment.centerRight,
                                  padding: const EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(10)),
                                      color: Theme.of(context)
                                          .secondaryHeaderColor),
                                  child: Text(_displayTexts[index]),
                                ),
                              ),
                              Positioned(
                                  left: -15,
                                  child: Icon(
                                    Icons.arrow_left,
                                    color:
                                        Theme.of(context).secondaryHeaderColor,
                                    size: 50,
                                  ))
                            ],
                          );
                  })),
          Padding(
              padding: const EdgeInsets.fromLTRB(10, 20, 10, 10),
              child: Row(
                children: [
                  Expanded(
                    child: TextInputField(
                      labelText: "Send a message",
                      controller: _textEditingController,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(10)),
                          color: Theme.of(context).primaryColor),
                      child: IconButton(
                          onPressed: () {
                            _sendMessageToBluetooth(
                                _textEditingController.text);
                            _textEditingController.text = "";
                          },
                          icon: const Icon(
                            Icons.send,
                            color: Colors.white,
                          )),
                    ),
                  )
                ],
              ))
        ],
      ),
    );
  }
}
