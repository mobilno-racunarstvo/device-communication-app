import 'package:device_connection_app/device/presentation/widget/Dashboard.dart';
import 'package:device_connection_app/device/presentation/widget/bluetooth_example/BluetoothConnectionCard.dart';
import 'package:device_connection_app/device/service/BluetoothService.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';

class BluetoothMain extends StatefulWidget {
  const BluetoothMain({super.key});

  @override
  State<BluetoothMain> createState() => _BluetoothMainState();
}

class _BluetoothMainState extends State<BluetoothMain> {
  BluetoothService bluetoothService = BluetoothService();
  List<BluetoothDevice> blDevices = [];

  @override
  void initState() {
    super.initState();
    getBlDevices();
  }

  void getBlDevices() async {
    var tempDevices = await bluetoothService.getPairedDevices();
    setState(() {
      blDevices = tempDevices;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).canvasColor,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => const Dashboard()));
          },
        ),
      ),
      body: Container(
          color: Theme.of(context).primaryColorDark,
          child: bluetoothService.error
              ? Align(
                  alignment: Alignment.center,
                  child: Text(
                    "An error occurred",
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .bodyMedium
                        ?.copyWith(color: Theme.of(context).errorColor),
                  ),
                )
              : blDevices.isNotEmpty
                  ? Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 30, bottom: 20),
                          child: Text(
                            "Devices to connect",
                            style: Theme.of(context)
                                .textTheme
                                .bodyLarge
                                ?.copyWith(fontWeight: FontWeight.bold),
                          ),
                        ),
                        Expanded(
                          child: ListView.builder(
                              itemCount: blDevices.length,
                              itemBuilder: (context, index) {
                                return BluetoothConnectionCard(
                                  bluetoothService: bluetoothService,
                                  blDevice: blDevices[index],
                                );
                              }),
                        )
                      ],
                    )
                  : const Align(
                      alignment: Alignment.center,
                      child: CircularProgressIndicator(
                        color: Colors.white,
                      ),
                    )),
    );
  }
}
