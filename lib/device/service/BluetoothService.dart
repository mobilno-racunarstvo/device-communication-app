import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:permission_handler/permission_handler.dart';

class BluetoothService{
  final FlutterBluetoothSerial _bluetooth = FlutterBluetoothSerial.instance;
  BluetoothConnection? connection;
  bool connected = false;
  bool error = false;

  bool get isConnected => connection != null && connection!.isConnected;

  Future<List<BluetoothDevice>> getPairedDevices() async {
    List<BluetoothDevice> devices = [];
    try {
      if (await Permission.bluetoothConnect.request().isGranted &&
          await Permission.bluetoothScan.request().isGranted &&
          await Permission.bluetooth.request().isGranted) {
        await FlutterBluetoothSerial.instance.requestEnable();
        devices = await _bluetooth.getBondedDevices();
      }
    } on PlatformException catch (e) {
      error = true;
    }
    return devices;
  }

  Future<void> connectToDevice(BluetoothDevice? bluetoothDevice) async {
    if (bluetoothDevice == null) {
    } else {
      if (!isConnected) {
        await BluetoothConnection.toAddress(bluetoothDevice.address)
            .then((connection) {
          this.connection = connection;
          debugPrint("Connected to the device");
          connected = true;
        }).catchError((error) {
          debugPrint("Cannot connect, exception occurred.");
          debugPrint(error);
          this.error = true;
        });
      } else {
        debugPrint("Device already connected.");
      }
    }
  }

  void listenForMessages(void Function(String, bool) function ){
    if(connection != null && connection?.input != null){
      connection!.input!.listen((event) {
        function(ascii.decode(event), false);
      }).onDone(() {debugPrint("Connection done");});
    }
  }

  void sendMessage(void Function(String, bool) function, String message){
    if(connection != null){
      connection!.output.add(ascii.encode(message));
      connection?.output.allSent.then((value) {
        debugPrint("Message sent!");
        function(message, true);
      }).onError((error, stackTrace) {
        debugPrint("Error when sending message");
      });
    }
  }

  void disposeConnection(){
    if (isConnected) {
      connection?.dispose();
      connection = null;
    }
  }
}