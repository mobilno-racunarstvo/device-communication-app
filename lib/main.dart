import 'package:device_connection_app/device/data/DeviceRepository.dart';
import 'package:device_connection_app/device/data/HttpDeviceRepository.dart';
import 'package:device_connection_app/device/presentation/controller/GetDeviceDataController.dart';
import 'package:device_connection_app/device/presentation/widget/Dashboard.dart';
import 'package:device_connection_app/shared/misc/Config.dart';
import 'package:device_connection_app/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;

String? getCurrentProfileURL() {
  return config[activeProfile.name]?['domain'];
}

final deviceRepositoryProvider = Provider<DeviceRepository>((ref) {
  return HttpDeviceRepository(client: http.Client(), domain: getCurrentProfileURL()!);
});

final getDeviceDataControllerProvider = StateNotifierProvider.autoDispose<
    GetDeviceDataController, AsyncValue<dynamic>>((ref) {
  return GetDeviceDataController(
    deviceRepository: ref.watch(deviceRepositoryProvider),
  );
});

void main() {
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Device connection',
      theme: theme,
      home: const Dashboard(),
    );
  }
}
