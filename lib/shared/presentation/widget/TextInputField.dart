import 'package:flutter/material.dart';
import 'package:gradient_borders/gradient_borders.dart';

class TextInputField extends StatelessWidget {
  const TextInputField(
      {super.key,
        this.labelText = "",
        this.obscureText = false,
        this.controller,
        this.validator,
        this.enabled = true,
        this.width = 400,
        this.height = 50,
        this.textAlign = TextAlign.start,
        this.onChanged,
        this.onTap,
        this.suffixText = "",
        this.style = const TextStyle(
            fontSize: 17.0,
            fontWeight: FontWeight.w700,
            fontFamily: 'Inter',
            letterSpacing: 0.1)});

  final String labelText;
  final bool obscureText;
  final TextEditingController? controller;
  final String? Function(String?)? validator;
  final bool enabled;
  final double width;
  final double height;
  final TextAlign textAlign;
  final void Function(String)? onChanged;
  final void Function()? onTap;
  final TextStyle? style;
  final String? suffixText;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: TextFormField(
        cursorColor: Theme.of(context).primaryColor,
        onTap: onTap,
        onChanged: onChanged,
        textAlign: textAlign,
        validator: validator,
        controller: controller,
        obscureText: obscureText,
        obscuringCharacter: "*",
        decoration: InputDecoration(
            suffixText: suffixText,
            enabled: enabled,
            errorMaxLines: 2,
            contentPadding:
            const EdgeInsets.symmetric(vertical: 16.0, horizontal: 10),
            filled: true,
            fillColor: Theme.of(context).canvasColor,
            border: GradientOutlineInputBorder(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Theme.of(context).primaryColorDark,
                      Theme.of(context).canvasColor,
                      Theme.of(context).canvasColor,
                    ]),
                width: 2,
                borderRadius: const BorderRadius.all(Radius.circular(12))),
            labelText: labelText,
            labelStyle: Theme.of(context).textTheme.bodyMedium?.copyWith(
                color: enabled
                    ? Theme.of(context).primaryColor
                    : Colors.grey)),
        style: style,
      ),
    );
  }
}

