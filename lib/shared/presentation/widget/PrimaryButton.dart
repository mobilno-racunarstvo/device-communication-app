import 'package:flutter/material.dart';

class PrimaryButton extends StatelessWidget {
  const PrimaryButton(
      {super.key,
        required this.onPressed,
        this.text,
        this.child,
        this.paddingLeft = 14.0,
        this.paddingBottom = 14.0,
        this.paddingRight = 14.0,
        this.paddingTop = 14.0,
        this.buttonRadius = 20.0,
        this.shadowOffset = const Offset(5, 5),
        this.color,
        this.width = 150
      });

  final void Function()? onPressed;
  final String? text;
  final Widget? child;
  final double paddingTop;
  final double paddingBottom;
  final double paddingLeft;
  final double paddingRight;
  final double buttonRadius;
  final Offset shadowOffset;
  final Color? color;
  final double? width;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(13)),
        boxShadow: [
          BoxShadow(
            color: color != null ? color!.withOpacity(0.15) : Theme.of(context).primaryColor.withOpacity(0.15),
            spreadRadius: 4,
            blurRadius: 4,
            offset: shadowOffset, // changes position of shadow
          ),
        ],
      ),
      child: ElevatedButton(
        onPressed: onPressed,
        style: ButtonStyle(
          backgroundColor: MaterialStatePropertyAll<Color>(color != null? color! : Theme.of(context).primaryColor),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                  side: BorderSide.none,
                  borderRadius: BorderRadius.circular(buttonRadius)
              )
          ),
        ),
        child: Padding(
          padding: EdgeInsets.fromLTRB(
              paddingLeft, paddingTop, paddingRight, paddingBottom),
          child: child ??
              Text(
                text ?? "",
                style: Theme.of(context).textTheme.bodyLarge,
              ),
        ),
      ),
    );
  }
}
