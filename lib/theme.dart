import 'package:flutter/material.dart';

final theme = ThemeData(
    brightness: Brightness.dark,
    primaryColor: const Color.fromRGBO(35, 169, 226, 1),
    primaryColorDark: const Color.fromRGBO(3, 16, 28, 1),
    cardColor: const Color.fromRGBO(32, 53, 73, 1),
    errorColor: const Color.fromRGBO(193, 54, 54, 1),
    canvasColor: const Color.fromRGBO(22, 41, 58, 1),
    textTheme: const TextTheme(
        bodySmall: TextStyle(fontFamily: "Inter", fontSize: 13),
        bodyMedium: TextStyle(fontFamily: "Inter", fontSize: 18),
        bodyLarge: TextStyle(fontFamily: "Inter", fontSize: 23)
    )
);